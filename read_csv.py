
import multiprocessing as mp
from multiprocessing import Process
import pandas as pd
from mrmg.logger import logger


def test_read(csv_file):
    logger.init()
    logger.warning('Reading {}'.format(csv_file))

    data = pd.read_csv(csv_file)

    logger.info('Data size for {0} [{1}, {2}]'.format(csv_file, data.shape[0], data.shape[1]))
    logger.info('Process for {} is done'.format(csv_file))

if __name__ == '__main__':
    logger.init()
    logger.set_level(logger.DEBUG)
    cpu_count = mp.cpu_count()
    logger.info('cpu count: {}'.format(cpu_count))
    logger.warning('main thread started')
    file_names = ['data_{}.csv'.format(i) for i in range(1, cpu_count + 1)]

    logger.debug('Serial processing: (직렬 프로세싱)')
    for file_name in file_names:
        test_read(file_name)

    logger.debug('Parallel processing: (병렬 프로세싱)')
    for file_name in file_names:
        p = Process(target=test_read, args=(file_name, ))
        p.start()

    logger.info('main thread done')